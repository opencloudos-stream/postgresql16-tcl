%global _build_id_links none
%global pgtclmajorversion 3.0
%global pgmajorversion 16

%{!?tcl_version: %global tcl_version %(echo 'puts $tcl_version' | tclsh)}
%{!?tcl_sitearch: %global tcl_sitearch %{_libdir}/tcl%{tcl_version}}

Summary:  A Tcl client library for PostgreSQL
Name:     postgresql%{pgmajorversion}-tcl
Version:  3.0.1
Release:  2%{?dist}
License:  BSD
URL:      https://github.com/flightaware/Pgtcl
Source0:  https://github.com/flightaware/Pgtcl/archive/v%{version}.tar.gz
	
BuildRequires:  make gcc autoconf 
BuildRequires:  postgresql%{pgmajorversion}-private-devel tcl-devel
# pg_config -> pg_server_config (postgresql-server-devel provides)
BuildRequires:	postgresql%{pgmajorversion}-server-devel
Requires: tcl(abi) >= 8.5

%description
PostgreSQL is an advanced Object-Relational database management system.
The tcl-pgtcl package contains Pgtcl, a Tcl client library for connecting
to a PostgreSQL server.

%prep
%autosetup -n Pgtcl-%{version}

autoconf

%build
%configure --libdir=%{tcl_sitearch} --with-tcl=%{_libdir} --disable-rpath
%{__make} all

%install
%make_install
# we don't really need to ship the .h file
%{__rm} -f %{buildroot}%{_includedir}/pgtclId.h

%files
%{_libdir}/tcl%{tcl_version}/libpgtcl.so
%{_libdir}/tcl%{tcl_version}/pgtcl%{pgtclmajorversion}/
%{_mandir}/mann/*

%changelog
* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 3.0.1-2
- Rebuilt for loongarch release

* Fri Apr 12 2024 Wang Guodong <gordonwwang@tencent.com> - 3.0.1-1
- init build
